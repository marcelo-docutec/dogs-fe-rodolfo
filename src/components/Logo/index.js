import React from "react";
import PropTypes from "prop-types";
import "./style.css";

/**
 * Component that returns an h1 to use as a title.
 */
export const Logo = ({ name }) => (
  <p className="app-logo">{name.toUpperCase()}</p>
);

Logo.propTypes = {
  name: PropTypes.string.isRequired
};

export default Logo;
